# Lipschitz_strain

# Lipschitz regularization using strain - Continuum setting

## Description
This is a simple 1D code implemented to test the Lipschitz regularization for materials that undergo damage. Presntly, the code is not very optimized, but fast enough since the example is in 1D.  

At earlier stages, the matlab subroutine fmincon with the sqp algorithm was used for the minimization. But it was found that the minimization was quite slow since the SQP algorithm evaluates the hessian using a finite difference method and this resulted in very large number of function evaluations. Hence, the code contains a simple implementaion of the SQP algorithm that uses an analytical hessian. This results in a drastic speedup. It shall be noted that interior-point method might have been used instead, which uses an analytical hessian, but the constraints are not strictly imposed in that method. How close we go to the constraints depends on the barrier parameter.

More to follow...
