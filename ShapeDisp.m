classdef ShapeDisp
   properties
      left,right
   end
   methods
      function s = shape_disp(obj,x)
          s=zeros(4,1);
          L=obj.right-obj.left;
          xi=(x-obj.left)/L;
          s(1) = 1-3.0*xi^2+2.0*xi^3;
          s(2) = L*(xi-2.0*xi^2+xi^3);
          s(3) =  3.0*xi^2-2.0*xi^3;
          s(4) = L*(-xi^2+xi^3);
      end
      function s = shape_disp_one_der(obj,x)
          s=zeros(4,1);
          L=obj.right-obj.left;
          xi=(x-obj.left)/L;
          s(1) = -6*xi+6.0*xi^2;
          s(2) = L*(1-4.0*xi+3*xi^2);
          s(3) =  6.0*xi-6.0*xi^2;
          s(4) = L*( -2*xi+3*xi^2);
          s = s/L;
      end
      function s = shape_disp_two_der(obj,x)
          s=zeros(4,1);
          L=obj.right-obj.left;
          xi=(x-obj.left)/L;
          s(1) = -6+12.0*xi;
          s(2) = L*(-4.0+6*xi);
          s(3) =  6.0-12.0*xi;
          s(4) = L*(-2+6*xi);
          s = s/(L^2);
      end
   end
end