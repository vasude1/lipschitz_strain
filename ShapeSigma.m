classdef ShapeSigma
   properties
      left,right
   end
   methods
      function s = shape_sigma_coeff(obj,x)
          l=obj.left;
          r=obj.right;
          s_ = [[1.0,l,l^2];[1.0,(l+r)/2.0,((l+r)^2)/4.0];[1.0,r,r^2]]';
          s=inv(s_);
      end
      function s = shape_sigma(obj,coeff,x)
          s= mtimes(coeff,[1.0;x;x*x]);
      end
      function s = shape_sigma_one_der(obj,coeff,x)
          s = mtimes(coeff, [0.0;1.0;2.0*x]);
      end
      function s = shape_sigma_two_der(obj,coeff,x)
          s = 2.0*coeff(:,3);
      end
   end
end