classdef ShapeTau
   properties
      left,right
   end
   methods
      function s = shape_tau_coeff(obj,x)
          l=obj.left;
          r=obj.right;
          s_ = [[1.0,l];[1.0,r]]';
          s=inv(s_);
      end
      function s = shape_tau(obj,coeff,x)
          s= mtimes(coeff,[1.0;x]);
      end
      function s = shape_tau_one_der(obj,coeff,x)
          s = 1.0*coeff(:,2);
      end
   end
end