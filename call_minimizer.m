function [final_solu,constraint_set,multipliers] = call_minimizer(displacement,ud,bfv,stiff,A_inequal,rhs_inequal,nelements,step,constraint_set,multipliers)
    [final_solu,constraint_set,multipliers] = solve_mechanical_prob(displacement,bfv,stiff,A_inequal,rhs_inequal,step,ud,constraint_set,multipliers);
end
