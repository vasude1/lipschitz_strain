function [lambda_arc,dlambda,damage_curr] = compute_inc_disp(curr_damage,curr_disp,prev_damage,prev_disp,B_strain_gp,delta_0,delta_f,E_0,weighed_area,M_integ_sigma,gp_to_nodes_sigma_t,lambda_arc_,inc_diss)
    Y_c = 0.5*100*15.0/1000.0;
    lambda_arc = lambda_arc_;
    
    nelements = int64(size(prev_damage,1)/3);
    nnodes_sigma = 3*nelements;
    nnodes_disp = 2*(nelements+1);
    big_residual = zeros(nnodes_sigma+1,1);
    big_matrix = zeros(nnodes_sigma+1,nnodes_sigma+1);
    
    damage_curr = curr_damage(:);

    epsilon_old = mtimes(B_strain_gp,prev_disp);
    curr_epsilon = mtimes(B_strain_gp,curr_disp);
    
    big_residual(1:end-1,1) = -curr_damage+((curr_epsilon>=delta_0) .* (curr_epsilon<=delta_f).*((curr_epsilon -delta_0)./(delta_f-delta_0))+(curr_epsilon>delta_f)*1.0);
    big_residual(end,1) = -Y_c*(mtimes(M_integ_sigma',gp_to_nodes_sigma_t)*(curr_damage-prev_damage))+inc_diss;
    
    big_matrix(1:end-1,1:end-1) = eye(nnodes_sigma); 
        
    E_alf_d = (1-damage_curr)./(1+(delta_f/delta_0-1)*damage_curr).*E_0;
    E_alf_w_area = weighed_area.*E_alf_d;
    E_alf_w_area_ = diag(reshape(E_alf_w_area,nnodes_sigma,1));
    stiff = mtimes(B_strain_gp',mtimes(E_alf_w_area_,B_strain_gp));

    C = zeros(nnodes_disp,nnodes_disp-2);
    for i=2:nnodes_disp-2
        C(i,i-1) = 1.0;
    end
    C(end,end)=1.0;
    u_f = zeros(nnodes_disp,1);
    u_f(end-1,1) = 1.0;
    CtKC_inv = inv(mtimes(C',mtimes(stiff,C)));
    dup_dlambda = -(mtimes(CtKC_inv,mtimes(C',stiff)))*u_f;
    du_dlambda = mtimes(C,dup_dlambda) + u_f;
    depsilon_dlambda = mtimes(B_strain_gp,du_dlambda);

    for i=1:nnodes_sigma
        big_matrix(i,end) = (curr_epsilon(i)<=delta_0)*0.0-(curr_epsilon(i)>=delta_0)*(curr_epsilon(i)<=delta_f)*depsilon_dlambda(i)/(delta_f-delta_0)+...
            (curr_epsilon(i)>=delta_f)*0.0;
    end
    big_matrix(end,1:end-1) = Y_c*(mtimes(M_integ_sigma',gp_to_nodes_sigma_t)); 

    sol = big_matrix\big_residual;
    lambda_arc = lambda_arc + sol(end,1);
    damage_curr = damage_curr + sol(1:end-1,1);
    damage_curr = (damage_curr>=prev_damage).*damage_curr + (damage_curr<prev_damage).*prev_damage+(damage_curr>=1.0).*1.0;
    
    dlambda=lambda_arc-lambda_arc_;

end