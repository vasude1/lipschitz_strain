function f = hessian_sqp(disp,multipliers,bfv,rhs,step,ud,disp_lag,constraint_set,P,stiff)
    ndof_disp = size(disp,1);
    nn=ndof_disp;
    u_active = size(find(constraint_set.u.active==1),1);
    f=zeros(ndof_disp+1+u_active,ndof_disp+1+u_active);
    % 1 to ndof_disp are the displacements 
    % ndof_disp +1 is the lagrnage multiplier that applies dirichlet bd
    % ndof_disp +2  to ndof_disp+1+u_active are inequality multipliers
    u=disp(:,1);    
    activeu = find(constraint_set.u.active==1);   
    f(1:nn,1:nn) = stiff;
    f(nn-1,nn+1) = 1.0;
    f(nn+1,nn-1) = f(nn-1,nn+1);
    
    if(u_active~=0)
        f(1:nn,nn+1+1:nn+1+u_active) = P(activeu,:)';
        f(nn+1+1:nn+1+u_active,1:nn) = P(activeu,:);
    end
end
