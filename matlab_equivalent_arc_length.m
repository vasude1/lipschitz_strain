% Parameters needed

delta_0 = 1.0/1000.0; % Strain at initiation
delta_f = 15.0/1000.0;  % Strain at failure
stiffness = 1E5;    % E_0
max_stress = stiffness*delta_0;
Y_c = 0.5*max_stress*delta_f;
max_strain = delta_f;
f=0.0; % Body force amplitude
rho=0.0;
nsteps=20000;   
length = 1.0;
nelements =105;
lc=0.2; % Lipschitz parameter. The strain gradient is \leq (delta_f-delta_0)/lc

%% Arrays needed

nnodes_disp = 2*(nelements+1);  % Number of DOF - Hermite elements
nnodes_sigma = 3*nelements; % Number of stresses - 3 gp per element
nnodes_tau = 2*nelements;   % Number of lagrange multipliers - 2 per element (piecewise linear and discontinuous)
nnodes_inequal = 2*nelements;   % Number of inequality constraints - 
                                   % is twice this number and equal to twice the number of multipliers

le = zeros(nelements,1); % Arrays containing element lengths - nonuniform mesh
nodal_coords = zeros(nelements+1,1);
% Update le array
curr_loc=0.0;
for i = 1:nelements
    if (i<=5)
        le(i,1) = 0.06;
        nodal_coords(i+1,1) = nodal_coords(i,1) + le(i,1);
    end
    if (i>nelements-5)
        le(i,1) =0.06;
        nodal_coords(i+1,1) = nodal_coords(i,1) +le(i,1);
    end
    if (i>5 && i<=nelements-5)
        le(i,1) = 0.4/(nelements-10);
        nodal_coords(i+1,1) = nodal_coords(i,1) + le(i,1);
    end
    curr_loc = curr_loc + nodal_coords(i+1,1)-nodal_coords(i,1);
end

C=stiffness;
E_0 = C*ones(nnodes_sigma,1);

displacement = zeros(nnodes_disp,1);
prev_displacement = zeros(nnodes_disp,1);
sigma = zeros(nnodes_sigma,1);
epsilon = zeros(nnodes_sigma,1);

damage = zeros(nnodes_sigma,1);
damage_curr = zeros(nnodes_sigma,1);
prev_damage = zeros(nnodes_sigma,1);
statev = zeros(nnodes_sigma,1);
prev_statev = zeros(nnodes_sigma,nsteps,1);

elem_mech_first = zeros(nnodes_sigma,nsteps,2);
elem_damage = zeros(nnodes_sigma,nsteps,1);
elem_mech_first_g = zeros(nnodes_sigma,nsteps,2);
displacement_all = zeros(nnodes_disp,nsteps,1);
epsilon_all = zeros(nelements+1,nsteps,1);
disp_dirichlet = zeros(nsteps,1);

lagrange_equal = zeros(nnodes_disp,1);
lagrange_equal_all = zeros(nnodes_disp,nsteps,1);
lagrange_inequal = zeros(2*nnodes_inequal,1);
lagrange_inequal_all = zeros(2*nnodes_inequal,nsteps,1);
A_inequal = zeros(2*nnodes_inequal,nnodes_disp);

%% Defining the arrays to convert gp values to nodal values and vice-versa

rt3 = sqrt(3);
rt5 = sqrt(5);

shape_Sigma = ShapeSigma;
shape_Sigma.left=-1.0;
shape_Sigma.right=1.0;

sigma_parent = shape_Sigma.shape_sigma_coeff(shape_Sigma.left);
gp_1 = shape_Sigma.shape_sigma(sigma_parent,-rt3/rt5);
gp_2 = shape_Sigma.shape_sigma(sigma_parent,0);
gp_3 = shape_Sigma.shape_sigma(sigma_parent,rt3/rt5);

nodes_to_gp_sigma = zeros(nnodes_sigma,nnodes_sigma);
gauss_weight = zeros(nnodes_sigma,1);

for i =1:nelements
    nodes_to_gp_sigma(3*i-2,3*i-2:3*i) = nodes_to_gp_sigma(3*i-2,3*i-2:3*i)+gp_1';
    nodes_to_gp_sigma(3*i-1,3*i-2:3*i) = nodes_to_gp_sigma(3*i-1,3*i-2:3*i)+gp_2';
    nodes_to_gp_sigma(3*i,3*i-2:3*i) = nodes_to_gp_sigma(3*i,3*i-2:3*i)+gp_3';

    gauss_weight(3*i-2,1) = 5.0/9.0;
    gauss_weight(3*i-1,1) = 8.0/9.0;
    gauss_weight(3*i,1) = 5.0/9.0;
end

gp_to_nodes_sigma = inv(nodes_to_gp_sigma);

%% Definition of rhs and area function

le_big = zeros(2*nnodes_inequal,1);
le_big(1:2:nnodes_inequal,1) = le(:,1);
le_big(2:2:nnodes_inequal,1) = le(:,1);
le_big(nnodes_inequal+1:2*nnodes_inequal,1) = le_big(1:nnodes_inequal,1);
rhs_inequal_init = zeros(2*nnodes_inequal,1); %+1
rhs_inequal_init(1:end) =(delta_f-delta_0)/lc/2.0* le_big(:,1).* ones(2*nnodes_inequal,1);

coords_gauss = zeros(nnodes_sigma,1);
for i =1:nelements
    coords_gauss(3*i-2) = nodal_coords(i,1);
    coords_gauss(3*i-1) = 0.5*(nodal_coords(i,1)+nodal_coords(i+1,1));
    coords_gauss(3*i) = nodal_coords(i+1,1);
end
coords_gauss = mtimes(nodes_to_gp_sigma,coords_gauss);
area_gauss = zeros(size(coords_gauss,1),1);
epsi = 0.12;
area = @(x) 1.0+(abs(x-0.5*length)<epsi) *0.01*cos(-pi/2/epsi*x+pi+pi/4/epsi);

for i = 1:size(coords_gauss,1)
    area_gauss(i,1) = area(coords_gauss(i,1));
end

weighed_area = zeros(nnodes_sigma,1);
for i = 1:nelements
    weighed_area(3*i-2,1) = area_gauss(3*i-2,1)*0.5*le(i,1)*gauss_weight(3*i-2,1);
    weighed_area(3*i-1,1) = area_gauss(3*i-1,1)*0.5*le(i,1)*gauss_weight(3*i-1,1);
    weighed_area(3*i,1) = area_gauss(3*i,1)*0.5*le(i,1)*gauss_weight(3*i,1);
end
%% Strain displacement - computation at gp and back to nodes

B_epsilon = zeros(nnodes_inequal,nnodes_disp);
Bb_u = zeros(nnodes_sigma,nnodes_disp);
B_u = zeros(2*nelements+1,nnodes_disp);

shape_disp = ShapeDisp;

for i = 1:nelements
    shape_disp.left = nodal_coords(i,1);
    shape_disp.right = nodal_coords(i+1,1);

    x=shape_disp.left;
    Bb_u(3*i-2,2*i-1:2*i+2) = shape_disp.shape_disp_one_der(x);
    B_u(2*i-1,2*i-1:2*i+2) = Bb_u(3*i-2,2*i-1:2*i+2);
    x=0.5*(1.0+1.0/rt3)*shape_disp.left+0.5*(1.0-1.0/rt3)*shape_disp.right;
    B_epsilon(2*i-1,2*i-1:2*i+2) = shape_disp.shape_disp_two_der(x);

    x=(shape_disp.left+shape_disp.right)/2;
    Bb_u(3*i-1,2*i-1:2*i+2) = shape_disp.shape_disp_one_der(x);
    B_u(2*i,2*i-1:2*i+2) = Bb_u(3*i-1,2*i-1:2*i+2);

    x=shape_disp.right;
    Bb_u(3*i,2*i-1:2*i+2) = shape_disp.shape_disp_one_der(x);
    B_u(2*i+1,2*i-1:2*i+2) = Bb_u(3*i,2*i-1:2*i+2);
    x=0.5*(1.0-1.0/rt3)*shape_disp.left+0.5*(1.0+1.0/rt3)*shape_disp.right;
    B_epsilon(2*i,2*i-1:2*i+2) = shape_disp.shape_disp_two_der(x);
end

B_strain_gp = mtimes(nodes_to_gp_sigma,Bb_u);

%% Constraint settings
% For active set SQP algorithm
field1 = 'active';  value1 = -1*ones(2*nnodes_inequal,1);
field2 = 'inactive';  value2 = ones(2*nnodes_inequal,1);
field3 = 'activeind'; value3 = [];
const_u_ = struct(field1,value1,field2,value2,field3,value3);

field1 = 'u';  value1 = const_u_;
const_u = struct(field1,value1);

field1 = 'u';  value1 = [];
field2 = 'dir';  value2 = 0;
multipliers = struct(field1,value1,field2,value2);

%% external calls

M_disp = rho*global_mass_disp(nodal_coords,nelements,area);
M_disp_no_iner = global_mass_disp(nodal_coords,nelements,area);
M_lagrange_ineq_init = global_lagrange_ineq(nodal_coords,nelements,area);
M_lagrange_ineq = M_lagrange_ineq_init(:,:);
M_integ_sigma = global_integ_sigma(nodal_coords,nelements,area);
rhs_inequal_init(1:nnodes_inequal) =(delta_f-delta_0)/lc/2.0* global_rhs(nodal_coords,nelements,area);
rhs_inequal_init(nnodes_inequal+1:2*nnodes_inequal) =(delta_f-delta_0)/lc/2.0* global_rhs(nodal_coords,nelements,area);
rhs_inequal = rhs_inequal_init(:);
%% Boundary conditions
% For displacement control
u_d = ones(nsteps,1);
u_d(1:150,1)=length*linspace(0.0, 0.4,150)/1000.0;
u_d(151:end,1) = length*linspace(0.4, 4.02,nsteps-150)/1000.0;
% Nodal body forces
bfv_ =  0.0*ones(nnodes_disp,1);
bfv_(1:2:end) = 0.0*sin(pi.*nodal_coords./0.15);
bfv_(2:2:end) = 0.0;
bfv = mtimes(M_disp_no_iner,bfv_);

%% The actual run

converge=0;
redo_step = 0;
iter=0;
count=0;
prev_lambda_arc = 1E-3;
lambda_arc = 0.0;
is_fully_damaged = 0;
D=0.0;

for step =3:nsteps
    fprintf('Step = %i \n',step);
    fprintf('u_d = %f \n',lambda_arc); %u_d(step,1)
        damage(:)=prev_statev(:,step-1,1);
        epsilon_curr = mtimes(nodes_to_gp_sigma,mtimes(Bb_u,displacement_all(:,step-1,1)));
        count=0;

    while(not(converge))
        prev_displacement(:) = displacement(:);
        epsilon = mtimes(Bb_u,displacement);
        epsilon_gp = mtimes(nodes_to_gp_sigma,epsilon);

        damage_ = (epsilon_gp>=delta_0) .* (epsilon_gp<=delta_f).*((epsilon_gp -delta_0)./(delta_f-delta_0))+(epsilon_gp>delta_f)*1.0;
        damage_temp = (damage_>prev_statev(:,step-1,1)).*damage_+(damage_<=prev_statev(:,step-1,1)).*prev_statev(:,step-1,1);
        damage_curr(:,1) = damage_temp(:,1);
        inc_diss = 0.002/1000.0;
        D= D + Y_c*sum(weighed_area.*(damage_curr-prev_statev(:,step-1,1)));

        % Path control from here
        if(D>0 && ~is_fully_damaged)
           [lambda_arc,dlambda,damage_curr_]=compute_inc_disp(damage_curr,displacement,prev_statev(:,step-1,1),displacement_all(:,step-1,1),B_strain_gp,delta_0,delta_f,...
               E_0,weighed_area,M_integ_sigma,gp_to_nodes_sigma,lambda_arc,inc_diss);
            damage_curr(:,1) = damage_curr_(:,1);
        end

        E_alf_d = (1-damage_curr)./(1+(delta_f/delta_0-1)*damage_curr).*E_0;
        E_alf_w_area = weighed_area.*E_alf_d;

        ud(1) = -u_d(step,1)*0.0;
        ud(2) = lambda_arc;

        damaged = find(damage_curr>=0.999);
        damaged_elem = unique(ceil(damaged/3));
        M_lagrange_ineq(:,:) = M_lagrange_ineq_init(:,:);
        rhs_inequal(:) = rhs_inequal_init(:);
        if(~isempty(damaged_elem)) % If an element is completely damaged, remove it from regularization
            fully_damaged = damaged_elem; 
            for elem = 1:size(fully_damaged,1)
                is_fully_damaged = 1;
                lambda_arc = lambda_arc + 0.0001/1000.0;
                M_lagrange_ineq(2*fully_damaged(elem)-1:2*fully_damaged(elem),:) = 0.0;
                activeu = find(const_u.u.active==1);  
                to_be_retained = find(activeu~= 2*fully_damaged(elem)-1);
                activeu_=activeu(to_be_retained);
                const_u.u.active(2*fully_damaged(elem)-1) = -1;
                const_u.u.inactive(2*fully_damaged(elem)-1) = 1;
                const_u.u.activeind = activeu_;
                multipliers.u = multipliers.u(to_be_retained);
                activeu = find(const_u.u.active==1);
                to_be_retained = find(activeu~= 2*fully_damaged(elem));
                activeu_=activeu(to_be_retained);
                const_u.u.active(2*fully_damaged(elem)) = -1;
                const_u.u.inactive(2*fully_damaged(elem)) = 1;
                const_u.u.activeind = activeu_;
                multipliers.u = multipliers.u(to_be_retained);
                
                
                activeu = find(const_u.u.active==1);  
                to_be_retained = find(activeu~= 2*fully_damaged(elem)-1+nnodes_inequal);
                activeu_=activeu(to_be_retained); 
                const_u.u.active(2*fully_damaged(elem)-1+nnodes_inequal) = -1;
                const_u.u.inactive(2*fully_damaged(elem)-1+nnodes_inequal) = 1;
                const_u.u.activeind = activeu_;
                multipliers.u = multipliers.u(to_be_retained);
                activeu = find(const_u.u.active==1);
                to_be_retained = find(activeu~= 2*fully_damaged(elem)+nnodes_inequal);
                activeu_=activeu(to_be_retained);
                const_u.u.active(2*fully_damaged(elem)+nnodes_inequal) = -1;
                const_u.u.inactive(2*fully_damaged(elem)+nnodes_inequal) = 1;
                const_u.u.activeind = activeu_;
                multipliers.u = multipliers.u(to_be_retained);
            end
        end
        A_inequal(1:nnodes_inequal,1:nnodes_disp) = M_lagrange_ineq;
        A_inequal(nnodes_inequal+1:2*nnodes_inequal,1:nnodes_disp) = -M_lagrange_ineq;
            
        E_alf_w_area_ = diag(reshape(E_alf_w_area,nnodes_sigma,1));
        stiff = mtimes(B_strain_gp',mtimes(E_alf_w_area_,B_strain_gp));
        
        [final_solu,constraint_set_,multipliers_] = call_minimizer(displacement,ud,bfv,stiff,A_inequal,rhs_inequal,nelements,step,const_u,multipliers);
    
        displacement = final_solu(1:nnodes_disp);
        const_u = constraint_set_;
        multipliers = multipliers_;
        
        epsilon = mtimes(Bb_u,displacement);
        epsilon_gp = mtimes(nodes_to_gp_sigma,epsilon);
        sigma = E_alf_d.*epsilon_gp;
        k = mtimes(B_epsilon,displacement);
        count = count+1;
        damage_ = (epsilon_gp>=delta_0) .* (epsilon_gp<=delta_f).*((epsilon_gp -delta_0)./(delta_f-delta_0))+(epsilon_gp>delta_f)*1.0;
        damage_temp = (damage_>prev_statev(:,step-1,1)).*damage_+(damage_<=prev_statev(:,step-1,1)).*prev_statev(:,step-1,1);
        damage_curr(:,1) = damage_temp(:,1);
        E_alf_d = (1-damage_curr)./(1+(delta_f/delta_0-1)*damage_curr).*E_0;
        sigma = E_alf_d.*epsilon_gp;
        damage_curr = (damage_curr>1.0).*1.0 + (damage_curr<=1.0).*damage_curr;
       if((prev_displacement-displacement)'*(M_disp_no_iner*(prev_displacement-displacement)) <1E-16  || count>100 )
            disp(max(epsilon));
            D= D + Y_c*(mtimes(M_integ_sigma',gp_to_nodes_sigma)*(damage_temp-prev_statev(:,step-1,1)));
            prev_statev(:,step,1) = damage_curr(:,1);
            prev_lambda_arc = lambda_arc;
            elem_mech_first(:,step,1) = epsilon(:);
            elem_mech_first(:,step,2) = mtimes(gp_to_nodes_sigma,sigma);
            elem_mech_first_g(:,step,1) = epsilon_gp(:);
            elem_mech_first_g(:,step,2) = sigma(:);
            disp_dirichlet(step,1) = lambda_arc;
            if(D==0)
                lambda_arc = lambda_arc+0.0005/1000.0;
            end
            converge=1;
            redo_step=0;
        end
    end

    displacement_all(:,step,1) = displacement(:);
    lagrange_inequal_all(const_u.u.activeind,step,1) = multipliers.u;

    converge=0;
end

