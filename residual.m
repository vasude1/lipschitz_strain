function f = residual(disp,multipliers,bfv,rhs,step,ud,disp_lag,constraint_set,P,stiff)
    ndof_disp = size(disp,1);
    u_active = size(find(constraint_set.u.active==1),1);
    f=zeros(ndof_disp+1+u_active,1);
    % 1 to ndof_disp are the displacements 
    % ndof_disp +1 is the lagrnage multiplier that applies dirichlet bd
    % ndof_disp +2  to ndof_disp+1+u_active are inequality multipliers
    
    u=disp(:,1);
     
    mu = disp_lag;
    forc_mu = zeros(ndof_disp,1);
    forc_mu(ndof_disp-1,1) = 1.0;
    activeu = find(constraint_set.u.active==1);
           
    temp = mtimes(stiff,u)+mu*forc_mu;
    if(u_active~=0)
        lambda = multipliers(:,1);
        temp = temp+mtimes(P(activeu,:)',lambda)-bfv + mu*forc_mu;
    end
    f(1:ndof_disp,1) = temp(:,1);
    f(ndof_disp+1,1) = u(ndof_disp-1)-ud(2);
  
    if(u_active~=0)
        f(ndof_disp+1+1:ndof_disp+1+u_active,1) = mtimes(P(activeu,:),u) - rhs(activeu,:);
    end

end